//
//  CodeDetail.swift
//  storecodes
//
//  Created by Rostás László on 21/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import Foundation

class CodeDetails {

    var id: Int?

    var type: String?

    var name: String?

    var data: String?

    var toValid: Date?

    var created: Date?

    var customerInfo: CustomerInfo?

    var storeDetail: StoreDetail?
}

class CustomerInfo {
    var id: Int?

    var externalId: String?

    var name: String?
}

class StoreDetail {
    var id: Int?

    var name: String?

    var longitudeGpsCoordinate: Double?

    var latitudeGpsCoordinate: Double?

    var address: String?
}

class AddCodeRequest {
    var storeId: Int?

    var type: String?

    var name: String?

    var data: String?

    var toValid: Date?
}
