//
//  CodeDetailService.swift
//  storecodes
//
//  Created by Rostás László on 21/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import Foundation
import CoreLocation

class CodeDetailService {

    private static var rootUrl = "http://92.249.254.18:15000"
    private static var loggedInUser = 1
    private static var _instance: CodeDetailService?

    private init() {
    }

    static func instance() -> CodeDetailService {
        if (_instance == nil) {
            _instance = CodeDetailService()
        }

        return _instance!
    }

    func getSites(delegate: @escaping ([StoreDetail])->()) {
        let url = "/rest/api/store/"
        getResponse(url: url, type: "GET") { (data: Data) in
            do {
                let json = try JSONSerialization.jsonObject(with: data) as? Array<[String: Any]>
                var results = [StoreDetail]()

                for store in json! {
                    let store1 = StoreDetail()
                    store1.address = store["address"] as! String?
                    store1.id = store["id"] as! Int?
                    store1.name = store["name"] as! String?

                    results.append(store1)
                }

                DispatchQueue.main.async { [unowned self] in
                    delegate(results)
                }
            } catch {
                print("Error deserializing JSON: \(error)")
            }
        }
    }

    func getOwnUsers(delegate: @escaping ([CustomerInfo])->()) {
        let url = "/rest/api/customerData/affectedCustomers/\(CodeDetailService.loggedInUser)"
        getResponse(url: url, type: "GET") { (data: Data) in
            do {
                let json = try JSONSerialization.jsonObject(with: data) as? Array<[String: Any]>
                var results = [CustomerInfo]()

                for customer in json! {
                    let customer1 = CustomerInfo()
                    customer1.externalId = customer["externalId"] as! String?
                    customer1.id = customer["id"] as! Int?
                    customer1.name = customer["name"] as! String?

                    results.append(customer1)
                }

                DispatchQueue.main.async { [unowned self] in
                    delegate(results)
                }
            } catch {
                print("Error deserializing JSON: \(error)")
            }
        }
    }


    func updateCode(updateCode: AddCodeRequest, userId: Int, delegate: ()->()) {
        let code = StoreCode()
        code.id = 1
        code.customerId = userId
        code.customerName = "Eladó felhasználó"
        code.data = updateCode.data
        code.name = updateCode.name
        code.storeId = Int(updateCode.storeId ?? 0)
        code.storeName = "Tesco Avas"
        code.type = updateCode.type

        delegate()
    }


    func addNewCode(newCode: AddCodeRequest, userId: Int, delegate: @escaping (StoreCode)->()) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"

        let urlPath = "rest/api/storecodes/\(userId)/\(CodeDetailService.loggedInUser)"
        let body = "{" +
            "\"name\" : \"\(newCode.name!)\"," +
            "\"type\" : \"\(newCode.type!)\"," +
            "\"storeId\" : \(newCode.storeId!)," +
            "\"toValid\" : \"\(formatter.string(from: newCode.toValid!))\"," +
            "\"data\" : \"\(newCode.data!)\"" +
        "}"

        let url = URL(string: "\(CodeDetailService.rootUrl)/\(urlPath)")!
        var request = URLRequest(url: url)
        request.timeoutInterval = 300
        request.httpBody = body.data(using: .utf8)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared

        session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (response as? HTTPURLResponse) != nil {
                let newCodeRawId = String.init(data: data!, encoding: .utf8)
                let newCodeId = Int(newCodeRawId!)

                let code = StoreCode()
                code.id = newCodeId
                code.customerId = userId
                code.customerName = "Eladó felhasználó"
                code.data = newCode.data
                code.name = newCode.name
                code.storeId = Int(newCode.storeId ?? 0)
                code.storeName = "Tesco Avas"
                code.type = newCode.type

                DispatchQueue.main.async { [unowned self] in
                    delegate(code)
                }
            }
        }).resume()
    }

    func deleteCode(codeId: Int, userId: Int, delegate: @escaping ()->()) {
        let url = "/rest/api/storecodes/\(CodeDetailService.loggedInUser)?codeId=\(codeId)"
        getResponse(url: url, type: "DELETE") { (data: Data) in
            DispatchQueue.main.async { [unowned self] in
                delegate()
            }
        }
    }

    func getDetail(id: Int, delegate: @escaping (CodeDetails)->()) {
        let url = "/rest/api/storecodes/detail/\(id)/\(CodeDetailService.loggedInUser)"
        getResponse(url: url, type: "GET") { (data: Data) in
            do {
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                let jsonStoreDetail = json!["storeDetail"] as! [String: Any]
                let jsonCustomerInfo = json!["customerInfo"] as! [String: Any]

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"

                let storeDetail = StoreDetail()
                storeDetail.address = jsonStoreDetail["address"] as! String?
                storeDetail.name = jsonStoreDetail["name"] as! String?
                storeDetail.id = jsonStoreDetail["id"] as! Int?
                storeDetail.latitudeGpsCoordinate = jsonStoreDetail["latitudeGpsCoordinate"] as! Double?
                storeDetail.longitudeGpsCoordinate = jsonStoreDetail["longitudeGpsCoordinate"] as! Double?

                let customerInfo = CustomerInfo()
                customerInfo.externalId = jsonCustomerInfo["externalId"] as! String?
                customerInfo.id = jsonCustomerInfo["id"] as! Int?
                customerInfo.name = jsonCustomerInfo["name"] as! String?

                let detail = CodeDetails()
                detail.id = id
                detail.data = json!["data"] as! String?
                detail.name = json!["name"] as! String?
                detail.type = json!["json"] as! String?
                detail.toValid = formatter.date(from: json!["toValid"] as! String)
                detail.created = formatter.date(from: json!["created"] as! String)
                detail.storeDetail = storeDetail
                detail.customerInfo = customerInfo


                DispatchQueue.main.async { [unowned self] in
                    delegate(detail)
                }
            } catch {
                print("Error deserializing JSON: \(error)")
            }
        }
    }

    func getNearlyCodes(coordinates: CLLocationCoordinate2D, delegate: @escaping ([StoreCode])->()) {
        let url = "/rest/api/storecodes?accountId=\(CodeDetailService.loggedInUser)&latitude=\(coordinates.latitude)&longitude=\(coordinates.longitude)&numberOfResults=40"
        getResponse(url: url, type: "GET") { (data: Data) in
            do {
                let json = try JSONSerialization.jsonObject(with: data) as? Array<[String: Any]>
                var results = [StoreCode]()

                for code in json! {
                    results.append(self.getStoreCode(data: code))
                }

                DispatchQueue.main.async { [unowned self] in
                    delegate(results)
                }
            } catch {
                print("Error deserializing JSON: \(error)")
            }
        }
    }

    private func getResponse(url: String, type: String, callback: @escaping (Data)->()) {
        let url = URL(string: "\(CodeDetailService.rootUrl)\(url)")!
        var request = URLRequest(url: url)
        request.timeoutInterval = 300
        request.httpMethod = type

        let session = URLSession.shared

        session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (response as? HTTPURLResponse) != nil {
                callback(data!)
            }
        }).resume()
    }

    private func getStoreCode(data: [String: Any]) -> StoreCode {
        let code = StoreCode()
        code.customerId = data["customerId"] as! Int?
        code.customerName = data["customerName"] as! String?
        code.data = data["data"] as! String?
        code.name = data["name"] as! String?
        code.storeId = data["storeId"] as! Int?
        code.storeName = data["storeName"] as! String?
        code.type = data["type"] as! String?
        code.id = data["id"] as! Int?

        return code
    }
}
