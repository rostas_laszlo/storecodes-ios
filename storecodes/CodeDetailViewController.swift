//
//  CodeDetailViewController.swift
//  storecodes
//
//  Created by Rostás László on 21/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import UIKit
import MapKit

class CodeDetailViewController: UIViewController {

    var selectedStoreCode: StoreCode?

    var loadedStoreCode: CodeDetails?

    @IBOutlet private var codeData: UILabel!

    @IBOutlet private var storeName: UILabel!

    @IBOutlet private var storeAddress: UILabel!

    @IBOutlet private var created: UILabel!

    @IBOutlet private var toValid: UILabel!

    @IBOutlet private var storeMap: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.codeData.text = selectedStoreCode!.data
        self.storeName.text = selectedStoreCode!.storeName
        self.title = selectedStoreCode!.customerName
        self.storeAddress.text = ""

        CodeDetailService.instance().getDetail(id: selectedStoreCode!.id!) { (detail: CodeDetails) in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"

            self.loadedStoreCode = detail
            self.codeData.text = detail.data
            self.created.text = formatter.string(from: detail.created!)
            self.toValid.text = formatter.string(from: detail.toValid!)
            self.storeAddress.text = detail.storeDetail?.address

            let distance: CLLocationDistance = 100
            let myCoordinate = CLLocationCoordinate2DMake(48.0760666, 20.7759506)
            let annotation = self.getMapAnnotation(coordinate: myCoordinate)
            let region = MKCoordinateRegionMakeWithDistance(myCoordinate, distance, distance)
            let adjustedRegion = self.storeMap.regionThatFits(region)

            self.storeMap.addAnnotation(annotation)
            self.storeMap.setRegion(adjustedRegion, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func getMapAnnotation(coordinate: CLLocationCoordinate2D) -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate;

        return annotation
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editCode" {
            let controller = segue.destination as! CodeEditorViewController

            controller.edtitingStoreCode = loadedStoreCode
        }
    }
}
