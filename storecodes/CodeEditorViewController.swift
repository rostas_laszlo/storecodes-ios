//
//  CodeEditorViewController.swift
//  storecodes
//
//  Created by Rostás László on 23/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import UIKit

class CodeEditorViewController: UIViewController, UITextFieldDelegate {

    private var selectedUserId: Int?

    private var selectedStoreId: Int?

    private var newId: StoreCode?

    @IBOutlet var deleteButton: UIButton!

    @IBOutlet var userSelectButton: UIButton!

    @IBOutlet var locationSelectButton: UIButton!

    @IBOutlet var toValidDate: UIDatePicker!

    @IBOutlet var formData: UITextField!

    @IBOutlet var formType: UITextField!

    @IBOutlet var formCodeName: UITextField!

    var edtitingStoreCode: CodeDetails?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let selectedStoreCode = edtitingStoreCode {
            self.selectedUserId = selectedStoreCode.customerInfo?.id
            self.selectedStoreId = selectedStoreCode.storeDetail?.id

            self.locationSelectButton.setTitle(selectedStoreCode.storeDetail?.name, for: .normal)
            self.userSelectButton.setTitle(selectedStoreCode.customerInfo?.name, for: .normal)
            self.userSelectButton.isEnabled = false
            self.toValidDate.date = selectedStoreCode.toValid ?? Date()
            self.formData.text = selectedStoreCode.data
            self.formType.text = selectedStoreCode.type
            self.deleteButton.isHidden = false
        } else {
            self.deleteButton.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocations" {
            let controller = segue.destination as! LocationSelectorTableViewController
            controller.selectedElement = { (selected: (StoreDetail)) in
                self.selectedStoreId = selected.id

                self.locationSelectButton.setTitle(selected.name, for: .normal)
                print(selected.name!)
            }
        } else if segue.identifier == "showUsers" {
            let controller = segue.destination as! UserSelectorTableViewController
            controller.selectedElement = { (selected: (CustomerInfo)) in
                self.selectedUserId = selected.id

                self.userSelectButton.setTitle(selected.name, for: .normal)
                print(selected.name!)
            }
        } else if segue.identifier == "showNewCode" {
            let controller = segue.destination as! CodeDetailViewController

            controller.selectedStoreCode = newId
            newId = nil
        }
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func delete() {
        guard let codeId = edtitingStoreCode?.id else { return }
        guard let userId = edtitingStoreCode?.customerInfo?.id else {return }

        CodeDetailService.instance().deleteCode(codeId: codeId, userId: userId) {
            let _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }

    @IBAction func save() {
        guard let userId = self.selectedUserId else {
            return
        }

        let newCodeDetails = AddCodeRequest()
        newCodeDetails.data = self.formData.text
        newCodeDetails.name = self.formCodeName.text
        newCodeDetails.storeId = self.selectedStoreId
        newCodeDetails.toValid = self.toValidDate.date
        newCodeDetails.type = self.formType.text

        if self.edtitingStoreCode == nil {
            CodeDetailService.instance().addNewCode(newCode: newCodeDetails, userId: userId) { (newId: StoreCode) in
                self.newId = newId
                self.performSegue(withIdentifier: "showNewCode", sender: self)
            }
        } else {
            CodeDetailService.instance().updateCode(updateCode: newCodeDetails, userId: userId) {
                self.newId = nil
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
