//
//  FirstViewController.swift
//  storecodes
//
//  Created by Rostás László on 17/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import UIKit
import CoreLocation

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let cellReuseIdentifier = "cell"

    private let loadingDataCellReuseIdentifier = "loadingData"

    private var loadingData = true

    private var result: [StoreCode] = []

    let searchController = UISearchController(searchResultsController: nil)

    private var locationManager: CLLocationManager?

    @IBOutlet var loadingView: UIView!

    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startSearchNearby()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loadingData {
            return 1
        } else {
            return result.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCell(withIdentifier: getIdentifier()) as UITableViewCell!

        if !loadingData {
            let code = result[indexPath.row]
            cell = getResultCodeCell(cell: cell as! ResultTableViewCell, code: code)
        }

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let indexPath = tableView.indexPathForSelectedRow
            let controller = segue.destination as! CodeDetailViewController

            controller.selectedStoreCode = result[indexPath!.row]
        }
    }

    func startSearchNearby() {
        self.loadingData = true
        self.tableView.reloadData()

        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters

        if #available(iOS 8.0, *)  {
            locationManager!.requestAlwaysAuthorization()
        }

        locationManager!.startUpdatingLocation()

    }

    func searchNearby(locations: [CLLocation]) {
        self.loadingData = true

        CodeDetailService.instance().getNearlyCodes(coordinates: locations[0].coordinate) { (result: [StoreCode]) in
            self.result = result
            self.loadingData = false
            self.tableView.reloadData()
        }
    }

    func searchCodes(seelctedFilter: Int, searchQuery: String?) {
        self.loadingData = true
        self.result = []
        var coordinates: CLLocationCoordinate2D = CLLocationCoordinate2D()
        coordinates.longitude = 1
        coordinates.latitude = 1

        CodeDetailService.instance().getNearlyCodes(coordinates: coordinates) { (result: [StoreCode]) in
            self.result = result
            self.loadingData = false
            self.tableView.reloadData()
        }

        self.tableView.reloadData()
    }

    private func getIdentifier() -> String {
        let id: String

        if loadingData {
            id = loadingDataCellReuseIdentifier
        } else {
            id = cellReuseIdentifier
        }

        return id
    }

    private func getResultCodeCell(cell: ResultTableViewCell, code actualCode: StoreCode) -> UITableViewCell {
        cell.customerLabel.text = actualCode.customerName
        cell.codeNameLabel.text = actualCode.name
        cell.dataLabel.text = actualCode.data
        cell.storeNameLabel.text = actualCode.storeName

        return cell
    }
}

extension FirstViewController: CLLocationManagerDelegate {
    // MARK: - CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        searchNearby(locations: locations)

        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false

        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["Mindenhol", "Telephely", "Felhasználó"]
        tableView.tableHeaderView = searchController.searchBar


    }
}

extension FirstViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("searchBar")

        self.searchCodes(seelctedFilter: searchBar.selectedScopeButtonIndex, searchQuery: searchBar.text!)
    }
}

extension FirstViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar

        if searchController.isActive {
            self.searchCodes(seelctedFilter: searchBar.selectedScopeButtonIndex, searchQuery: searchBar.text!)
        } else {
            //self.startSearchNearby()
        }

        print("updateSearchResults")
    }
}
