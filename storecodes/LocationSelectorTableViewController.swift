//
//  LocationSelectorTableViewController.swift
//  storecodes
//
//  Created by Rostás László on 23/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import UIKit

class LocationSelectorTableViewController: UITableViewController {

    private var sites: [StoreDetail]? = nil

    var selectedElement: ((StoreDetail)->())? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        CodeDetailService.instance().getSites { (results: [StoreDetail]) in
            self.sites = results
            self.tableView.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let site = sites![indexPath.row]
        selectedElement?(site)

        print("You tapped cell number \(indexPath.row).")
        let _ = navigationController?.popViewController(animated: true)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sites?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let site = sites![indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel!.text = site.name
        cell.detailTextLabel!.text = site.address

        return cell
    }
}
