//
//  StoreCode.swift
//  storecodes
//
//  Created by Rostás László on 22/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import Foundation

class StoreCode {
    var id: Int?

    var name: String?

    var type: String?

    var storeName: String?

    var storeId: Int?

    var customerName: String?

    var customerId: Int?

    var data: String?
}


