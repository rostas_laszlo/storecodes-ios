//
//  LocationSelectorTableViewController.swift
//  storecodes
//
//  Created by Rostás László on 23/04/17.
//  Copyright © 2017 storecodes. All rights reserved.
//

import UIKit

class UserSelectorTableViewController: UITableViewController {

    private var customers: [CustomerInfo]? = nil

    var selectedElement: ((CustomerInfo)->())? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        CodeDetailService.instance().getOwnUsers(delegate: { (results: [CustomerInfo]) in
            self.customers = results
            self.tableView.reloadData()
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customer = customers![indexPath.row]
        selectedElement?(customer)

        print("You tapped cell number \(indexPath.row).")
        let _ = navigationController?.popViewController(animated: true)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customer = customers![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel!.text = customer.name

        return cell
    }
}
